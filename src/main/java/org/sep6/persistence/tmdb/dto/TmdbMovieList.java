package org.sep6.persistence.tmdb.dto;

import java.util.List;

import org.sep6.domain.Movie;

public record TmdbMovieList(
		List<Movie> results
) {}
