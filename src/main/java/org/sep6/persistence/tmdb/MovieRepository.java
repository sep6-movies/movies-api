package org.sep6.persistence.tmdb;

import org.sep6.domain.Credits;
import org.sep6.domain.Movie;
import org.sep6.persistence.tmdb.dto.TmdbMovieList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "tmdbMovieClient", url = "https://api.themoviedb.org/3")
public interface MovieRepository {

	@GetMapping("/movie/{movieId}")
	Movie findById(@PathVariable("movieId") String movieId);

	@GetMapping("/search/movie")
	TmdbMovieList findByTitle(@RequestParam("query") String title);

	@GetMapping("/movie/popular")
	TmdbMovieList getPopular();

	@GetMapping("/movie/{movieId}/credits")
	Credits getCredits(@PathVariable("movieId") String movieId);
}
