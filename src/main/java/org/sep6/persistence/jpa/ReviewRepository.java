package org.sep6.persistence.jpa;

import java.util.List;

import org.sep6.domain.Rating.RatingId;
import org.sep6.domain.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReviewRepository extends JpaRepository<Review, RatingId> {

	@Query("SELECT r FROM Review r WHERE r.id.movieId = :movieId")
	List<Review> findByMovieId(@Param("movieId") String movieId);
}
