package org.sep6.persistence.jpa;

import java.util.List;

import org.sep6.domain.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RatingRepository extends JpaRepository<Rating, Rating.RatingId> {

	@Query("SELECT r FROM Rating r WHERE r.id.movieId = :movieId")
	List<Rating> findByMovieId(@Param("movieId") String movieId);
}
