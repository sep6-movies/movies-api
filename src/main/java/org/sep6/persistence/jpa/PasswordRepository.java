package org.sep6.persistence.jpa;

import java.util.Optional;

import org.sep6.domain.Password;
import org.sep6.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordRepository extends JpaRepository<Password, Long> {

	Optional<Password> findByUser(User user);
}
