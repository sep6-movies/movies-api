package org.sep6.service;

import java.util.Optional;

import org.sep6.domain.Password;
import org.sep6.domain.User;
import org.sep6.persistence.jpa.PasswordRepository;
import org.sep6.persistence.jpa.UserRepository;
import org.sep6.security.AuthService;
import org.sep6.security.UserPrincipalDto;
import org.sep6.service.dto.RegisterUserDto;
import org.sep6.service.dto.UpdateUserDto;
import org.sep6.service.dto.UserDetailsDto;
import org.sep6.service.exception.EmailInUseException;
import org.sep6.service.exception.NotAuthorizedException;
import org.sep6.service.exception.UsernameInUseException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

	private final UserRepository userRepo;
	private final PasswordRepository passwordRepo;
	private final PasswordEncoder passwordEncoder;
	private final AuthService authService;

	@Transactional
	public UserDetailsDto register(RegisterUserDto dto) {
		validateEmail(dto.email());
		validateUsername(dto.username());

		User user = userRepo.save(new User(dto.username(), dto.email()));
		passwordRepo.save(new Password(user,
					passwordEncoder.encode(dto.password())));

		return toDetails(user);
	}

	public Optional<UserDetailsDto> getDetails(long id) {
		return userRepo.findById(id)
			.map(this::toDetails);
	}

	public Optional<UserDetailsDto> update(long id, UpdateUserDto dto, Authentication auth) {
		authService.authenticated(id, auth)
			.orElseThrow(NotAuthorizedException::new);

		return userRepo.findById(id)
			.map(user -> applyUpdate(user, dto))
			.map(userRepo::save)
			.map(this::toDetails);
	}

	private User applyUpdate(User u, UpdateUserDto dto) {
		if(null != dto.email()) {
			validateEmail(dto.email());
			u.setEmail(dto.email());
		}

		if(null != dto.username()) {
			validateUsername(dto.username());
			u.setUsername(dto.username());
		}

		if(null != dto.password()) {
			var password = passwordRepo.findByUser(u)
				.orElseThrow(() -> new IllegalArgumentException("User does not have a password"));

			password.setValue(dto.password());
			passwordRepo.save(password);
		}

		return u;
	}

	private void validateEmail(String email)  {
		if (userRepo.findByEmail(email).isPresent())
			throw new EmailInUseException(email);
	}

	private void validateUsername(String username) {
		if (userRepo.findByUsername(username).isPresent())
			throw new UsernameInUseException(username);
	}

	private UserDetailsDto toDetails(User entity) {
		return new UserDetailsDto(
				entity.getId(),
				entity.getUsername(),
				entity.getEmail());
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepo.findByUsername(username)
				.flatMap(u -> passwordRepo.findByUser(u)
						.map(p -> new UserPrincipalDto(toDetails(u), p.getValue())))
				.orElseThrow(() -> new UsernameNotFoundException(username));
	}
}
