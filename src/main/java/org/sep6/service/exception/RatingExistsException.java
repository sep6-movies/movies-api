package org.sep6.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class RatingExistsException extends RuntimeException {

	public RatingExistsException() {
		super("Rating already exists");
	}
}
