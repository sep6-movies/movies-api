package org.sep6.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UsernameInUseException extends RuntimeException {

	public UsernameInUseException(String username) {
		super(buildMessage(username));
	}

	public UsernameInUseException(String username, Throwable cause) {
		super(buildMessage(username), cause);
	}

	private static String buildMessage(String username) {
		return "Username is already in use: " + username;
	}
}
