package org.sep6.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmailInUseException extends RuntimeException {

	public EmailInUseException(String email) {
		super(buildMessage(email));
	}

	public EmailInUseException(String email, Throwable cause) {
		super(buildMessage(email), cause);
	}

	private static String buildMessage(String email) {
		return "Email is already in use: " + email;
	}
}
