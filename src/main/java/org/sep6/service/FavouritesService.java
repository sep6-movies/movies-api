package org.sep6.service;

import java.util.List;
import java.util.Optional;

import org.sep6.domain.User;
import org.sep6.persistence.jpa.UserRepository;
import org.sep6.persistence.tmdb.MovieRepository;
import org.sep6.security.AuthService;
import org.sep6.service.dto.AddToFavouritesDto;
import org.sep6.service.dto.MovieSummaryDto;
import org.sep6.service.exception.NotAuthorizedException;
import org.sep6.service.exception.NotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FavouritesService {

	private final UserRepository userRepo;
	private final AuthService authService;
	private final MovieRepository movieRepo;
	private final MovieService movieService;

	public void addToFavourites(long userId, AddToFavouritesDto dto, Authentication auth) {
		var user = authService.authenticated(userId, auth)
			.orElseThrow(NotAuthorizedException::new);

		if(null == movieRepo.findById(dto.movieId()))
			throw new NotFoundException();

		user.getFavouriteMovies().add(dto.movieId());
		userRepo.save(user);
	}

	public Optional<List<MovieSummaryDto>> getFavourites(long userId, Authentication auth) {
		return authService.authenticated(userId, auth)
			.map(User::getFavouriteMovies)
			.map(ids -> ids.stream()
					.map(this::getSummary)
					.toList());
	}

	private MovieSummaryDto getSummary(String id) {
		return Optional.of(movieRepo.findById(id))
			.map(movieService::toSummary)
			.orElseThrow(NotFoundException::new);
	}

	public void removeFromFavourites(long userId, String movieId, Authentication auth) {
		var user = authService.authenticated(userId, auth)
			.orElseThrow(NotAuthorizedException::new);

		if(!user.getFavouriteMovies().remove(movieId))
			throw new NotFoundException();

		userRepo.save(user);
	}
}
