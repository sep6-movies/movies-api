package org.sep6.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.sep6.domain.Rating;
import org.sep6.domain.Rating.RatingId;
import org.sep6.persistence.jpa.RatingRepository;
import org.sep6.persistence.tmdb.MovieRepository;
import org.sep6.security.AuthService;
import org.sep6.service.dto.AddRatingDto;
import org.sep6.service.dto.RatingDto;
import org.sep6.service.dto.RatingSummaryDto;
import org.sep6.service.exception.NotAuthorizedException;
import org.sep6.service.exception.RatingExistsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RatingService {

	private final RatingRepository ratingRepo;
	private final MovieRepository movieRepo;
	private final AuthService authService;

	public Optional<RatingSummaryDto> addRating(String movieId, AddRatingDto dto, Authentication auth) {
		var user = authService.authenticated(dto.userId(), auth)
			.orElseThrow(NotAuthorizedException::new);

		ratingRepo.findById(new RatingId(user.getId(), movieId))
			.ifPresent(r -> { throw new RatingExistsException(); });

		return Optional.of(movieId)
			.filter(id -> null != movieRepo.findById(id))
			.map(id -> new Rating(user, movieId, dto.value()))
			.map(ratingRepo::save)
			.flatMap(r -> getSummary(r.getMovieId()));
	}

	public Optional<RatingDto> getRating(String movieId, long userId) {
		return ratingRepo.findById(new RatingId(userId, movieId))
			.map(this::toDto);
	}

	public Optional<RatingSummaryDto> deleteRating(String movieId, long userId, Authentication auth) {
		var id = authService.authenticated(userId, auth)
			.map(p -> new RatingId(p.getId(), movieId))
			.orElseThrow(NotAuthorizedException::new);

		return ratingRepo.findById(id)
			.map(r -> {
				ratingRepo.delete(r);
				return r.getMovieId();
			})
			.flatMap(this::getSummary);
	}

	public Optional<RatingSummaryDto> updateRating(String movieId, long userId, RatingDto dto, Authentication auth) {

		var id = authService.authenticated(userId, auth)
			.map(p -> new RatingId(p.getId(), movieId))
			.orElseThrow(NotAuthorizedException::new);

		return Optional.of(id)
			.flatMap(ratingRepo::findById)
			.map(r -> r.withValue(dto.value()))
			.map(ratingRepo::save)
			.flatMap(r -> getSummary(r.getMovieId()));
	}

	public Optional<RatingSummaryDto> getSummary(String movieId) {
		return Optional.ofNullable(ratingRepo.findByMovieId(movieId))
			.map(ratings -> new RatingSummaryDto(
						getAverage(ratings),
						countRatings(ratings)));
	}

	public Optional<Double> getAverageRating(String movieId) {
		return Optional.of(ratingRepo.findByMovieId(movieId))
			.map(this::getAverage);
	}

	private Map<Integer, Long> countRatings(List<Rating> ratings) {
		return ratings.stream()
			.map(Rating::getValue)
			.collect(Collectors.groupingBy(
						Function.identity(), Collectors.counting()));
	}

	private double getAverage(List<Rating> ratings) {
		return ratings.stream()
			.mapToInt(Rating::getValue)
			.average()
			.orElse(0);
	}

	private RatingDto toDto(Rating r) {
		return new RatingDto(r.getValue());
	}
}