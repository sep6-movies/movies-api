package org.sep6.service;

import java.util.List;
import java.util.Optional;

import org.sep6.domain.CastMember;
import org.sep6.domain.Credits;
import org.sep6.domain.CrewMember;
import org.sep6.domain.Movie;
import org.sep6.persistence.tmdb.MovieRepository;
import org.sep6.service.dto.MovieDetailsDto;
import org.sep6.service.dto.MovieSummaryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MovieService {

	private final MovieRepository movieRepo;
	private final RatingService ratingService;

	@Value("${tmdb.imagePrefix}")
	private String imageUrlPrefix;

	public List<MovieSummaryDto> getPopular() {
		return movieRepo.getPopular()
			.results()
			.stream()
			.map(this::toSummary)
			.toList();
	}

	public Optional<MovieDetailsDto> getDetails(String movieId) {
		return Optional.of(movieRepo.findById(movieId))
			.map(this::toDetails);
	}

	public List<MovieSummaryDto> searchByTitle(String phrase) {
		return movieRepo.findByTitle(phrase)
			.results()
			.stream()
			.map(this::toSummary)
			.toList();
	}

	MovieDetailsDto toDetails(Movie movie) {
		Credits credits = movieRepo.getCredits(movie.id());

		return ratingService.getSummary(movie.id())
			.map(ratings -> new MovieDetailsDto(
				movie.id(),
				movie.title(),
				buildImageUrl(movie.posterPath()),
				movie.description(),
				credits.getCast().stream().map(this::withFullImagePath).toList(),
				credits.getCrew().stream().map(this::withFullImagePath).toList(),
				ratings))
			.orElseThrow();
	}

	MovieSummaryDto toSummary(Movie movie) {
		return ratingService.getAverageRating(movie.id())
			.map(avg -> new MovieSummaryDto(
				movie.id(),
				movie.title(),
				buildImageUrl(movie.posterPath()),
				movie.description(),
				avg))
			.orElseThrow();
	}

	private CastMember withFullImagePath(CastMember member) {
		return new CastMember(
				member.id(),
				member.name(),
				buildImageUrl(member.picture()),
				member.character());
	}

	private CrewMember withFullImagePath(CrewMember member) {
		return new CrewMember(
				member.id(),
				member.name(),
				buildImageUrl(member.picture()),
				member.job());
	}

	private String buildImageUrl(String path) {
		if(null == path)
			return null;

		return String.format("%s%s", imageUrlPrefix, path);
	}
}
