package org.sep6.service.dto;

import org.hibernate.validator.constraints.Range;

public record AddRatingDto(
		long userId,

		@Range(min = 1, max = 5, message = "Rating has to be between 1 and 5")
		int value
) {}
