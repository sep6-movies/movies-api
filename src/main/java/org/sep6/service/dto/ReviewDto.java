package org.sep6.service.dto;

import jakarta.validation.constraints.NotBlank;

public record ReviewDto(
		long reviewerId,
		String reviewer,
		String movieId,
		String title,
		String content
) {}
