package org.sep6.service.dto;

import org.hibernate.validator.constraints.Range;

public record RatingDto(

		@Range(min = 1, max = 5, message = "Rating has to be a number from 1 to 5")
		int value
) {}