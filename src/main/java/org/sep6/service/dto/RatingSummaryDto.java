package org.sep6.service.dto;

import java.util.Map;

public record RatingSummaryDto(
		double avg,
		Map<Integer, Long> counts
) {}