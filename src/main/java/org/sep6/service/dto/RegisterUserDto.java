package org.sep6.service.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record RegisterUserDto(
		@NotBlank(message = "Username cannot be blank")
		@Size(min = 5, max = 15, message = "Username should be 5 to 15 characters long")
		String username,

		@NotBlank(message = "Email cannot be blank")
		@Email(message = "Invalid email")
		String email,

		@NotBlank(message = "Password cannot be blank")
		@Size(min = 3, max = 50, message = "Password should be 3 to 50 characters long")
		String password
) {}