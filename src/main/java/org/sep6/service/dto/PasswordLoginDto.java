package org.sep6.service.dto;

import jakarta.validation.constraints.NotBlank;

public record PasswordLoginDto(
		@NotBlank(message = "Provide username")
		String username,

		@NotBlank(message = "Provide password")
		String password
) {}
