package org.sep6.service.dto;

import jakarta.validation.constraints.NotBlank;

public record AddReviewDto(
		@NotBlank
		String title,

		@NotBlank
		String content
) {}
