package org.sep6.service.dto;

public record MovieSummaryDto(
		String id,
		String title,
		String posterUrl,
		String description,
		double avgRating
) {}