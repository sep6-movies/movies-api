package org.sep6.service.dto;

public record UserDetailsDto(
		long id,
		String username,
		String email
) {}
