package org.sep6.service.dto;

import java.util.List;

import org.sep6.domain.CastMember;
import org.sep6.domain.CrewMember;

public record MovieDetailsDto(
		String id,
		String title,
		String posterUrl,
		String description,
		List<CastMember> cast,
		List<CrewMember> crew,
		RatingSummaryDto rating
) {}
