package org.sep6.service.dto;

public record AddToFavouritesDto(
		String movieId
) {}
