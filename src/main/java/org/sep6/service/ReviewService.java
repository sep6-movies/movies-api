package org.sep6.service;

import java.util.List;
import java.util.Optional;

import org.sep6.domain.Rating.RatingId;
import org.sep6.domain.Review;
import org.sep6.persistence.jpa.RatingRepository;
import org.sep6.persistence.jpa.ReviewRepository;
import org.sep6.persistence.tmdb.MovieRepository;
import org.sep6.security.AuthService;
import org.sep6.service.dto.ReviewDto;
import org.sep6.service.exception.NotAuthorizedException;
import org.sep6.service.exception.NotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ReviewService {

	private final ReviewRepository reviewRepo;
	private final RatingRepository ratingRepo;
	private final MovieRepository movieRepo;
	private final AuthService authService;

	public Optional<ReviewDto> setReview(String movieId, long reviewerId, ReviewDto dto, Authentication auth) {
		var id = authService.authenticated(reviewerId, auth)
			.map(p -> new RatingId(p.getId(), movieId))
			.orElseThrow(NotAuthorizedException::new);

		return ratingRepo.findById(id)
			.flatMap(r -> Optional.ofNullable(r.getReview())
					.map(rv -> apply(rv, dto))
					.or(() -> Optional.of(new Review(r, dto.title(), dto.content()))))
			.map(reviewRepo::save)
			.map(this::toDto);
	}

	public Optional<List<ReviewDto>> getReviews(String movieId) {
		return Optional.ofNullable(movieRepo.findById(movieId))
			.map(m -> reviewRepo.findByMovieId(movieId)
					.stream()
					.map(this::toDto)
					.toList());
	}

	public Optional<ReviewDto> getReview(String movieId, long reviewerId) {
		return reviewRepo.findById(new RatingId(reviewerId, movieId))
			.map(this::toDto);
	}

	public void removeReview(String movieId, long reviewerId, Authentication auth) {
		authService.authenticated(reviewerId, auth)
			.orElseThrow(NotAuthorizedException::new);

		reviewRepo.findById(new RatingId(reviewerId, movieId))
			.map(Review::getRating)
			.map(r -> r.withReview(null))
			.map(ratingRepo::save)
			.orElseThrow(NotFoundException::new);
	}

	private Review apply(Review r, ReviewDto dto) {
		r.setTitle(dto.title());
		r.setContent(dto.content());

		return r;
	}

	private ReviewDto toDto(Review entity) {
		return new ReviewDto(
				entity.getReviewer().getId(),
				entity.getReviewer().getUsername(),
				entity.getMovieId(),
				entity.getTitle(),
				entity.getContent());
	}
}
