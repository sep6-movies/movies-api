package org.sep6.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CastMember(
		int id,

		String name,

		@JsonProperty("profile_path")
		String picture,

		String character
) {}
