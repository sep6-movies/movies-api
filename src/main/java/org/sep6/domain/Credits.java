package org.sep6.domain;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Credits {

	List<CastMember> cast;
	List<CrewMember> crew;
}
