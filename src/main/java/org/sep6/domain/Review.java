package org.sep6.domain;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Column;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Review {

	@Id
	private Rating.RatingId id;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@MapsId
	private Rating rating;

	private String title;

	@Column(columnDefinition = "TEXT")
	private String content;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Vote> votes;

	public Review(Rating rating, String title, String content) {
		this.rating = rating;
		this.title = title;
		this.content = content;
		this.votes = new ArrayList<>();
	}

	public User getReviewer() {
		return rating.getReviewer();
	}

	public String getMovieId() {
		return rating.getMovieId();
	}
}