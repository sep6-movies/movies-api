package org.sep6.domain;

import java.io.Serializable;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Vote {

	@Embeddable
	@Data
	static class VoteId implements Serializable {
		private Rating.RatingId reviewId;
		private long voterId;
	}

	@EmbeddedId
	private VoteId id;

	@ManyToOne
	@MapsId("reviewId")
	private Review review;

	@ManyToOne
	@MapsId("voterId")
	private User voter;

	private boolean upvote;
}