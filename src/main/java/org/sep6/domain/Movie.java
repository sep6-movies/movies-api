package org.sep6.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public record Movie(
		String id,
		String title,
		@JsonProperty("poster_path") String posterPath,
		@JsonProperty("overview") String description
) {}