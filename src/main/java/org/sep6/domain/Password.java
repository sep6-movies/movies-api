package org.sep6.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Password {

	@Id
	private Long id;

	@OneToOne
	@MapsId
	private User user;

	@Column(name = "password_value")
	private String value;

	public Password(User user, String value) {
		this.user = user;
		this.value = value;
	}
}
