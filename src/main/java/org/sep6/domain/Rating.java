package org.sep6.domain;

import java.io.Serializable;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Column;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Rating {

	@Embeddable
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class RatingId implements Serializable {
		private long userId;
		private String movieId;
	}

	@EmbeddedId
	private RatingId id;

	@ManyToOne
	@MapsId("userId")
	private User reviewer;

	@Column(name = "rating_value")
	private int value;

	@OneToOne(mappedBy = "rating", cascade = CascadeType.ALL, orphanRemoval = true, optional = true, fetch = FetchType.LAZY)
	private Review review;

	public Rating(User reviewer, String movieId, int value) {
		this.id = new RatingId(reviewer.getId(), movieId);
		this.reviewer = reviewer;
		this.value = value;
	}

	public Rating withValue(int value) {
		this.value = value;
		return this;
	}

	public Rating withReview(Review review) {
		this.review = review;
		return this;
	}

	public String getMovieId() {
		return id.movieId;
	}
}