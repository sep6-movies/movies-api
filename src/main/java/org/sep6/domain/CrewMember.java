package org.sep6.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CrewMember(
		int id,

		String name,

		@JsonProperty("profile_path")
		String picture,

		String job
) {}