package org.sep6.controller.rest;

import java.util.List;

import org.sep6.service.ReviewService;
import org.sep6.service.dto.ReviewDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/movies/{movieId}/reviews")
@RequiredArgsConstructor
public class ReviewController {

	private final ReviewService reviewService;

	@GetMapping
	public ResponseEntity<List<ReviewDto>> getReviews(
			@PathVariable("movieId") String movieId) {

		return ResponseEntity.of(reviewService.getReviews(movieId));
	}

	@GetMapping("/{reviewerId}")
	public ResponseEntity<ReviewDto> getReview(
			@PathVariable("movieId") String movieId,
			@PathVariable("reviewerId") long reviewer) {

		return ResponseEntity.of(reviewService.getReview(movieId, reviewer));
	}

	@PutMapping("/{reviewerId}")
	public ResponseEntity<ReviewDto> setReview(
			@PathVariable("movieId") String movieId,
			@PathVariable("reviewerId") long reviewer,
			@RequestBody @Valid ReviewDto dto,
			Authentication auth) {

		return ResponseEntity.of(reviewService.setReview(
					movieId, reviewer, dto, auth));
	}

	@DeleteMapping("/{reviewerId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeReview(
			@PathVariable("movieId") String movieId,
			@PathVariable("reviewerId") long reviewer,
			Authentication auth) {

		reviewService.removeReview(movieId, reviewer, auth);
	}
}
