package org.sep6.controller.rest;

import java.util.List;

import org.sep6.service.MovieService;
import org.sep6.service.dto.MovieDetailsDto;
import org.sep6.service.dto.MovieSummaryDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor
public class MovieController {

	private final MovieService service;

	@GetMapping("/popular")
	public List<MovieSummaryDto> getPopular() {
		return service.getPopular();
	}

	@GetMapping("/{movieId}")
	public ResponseEntity<MovieDetailsDto> getDetails(@PathVariable("movieId") String movieId) {

		return ResponseEntity.of(service.getDetails(movieId));
	}

	@GetMapping("/search")
	public List<MovieSummaryDto> searchByTitle(@RequestParam("query") String phrase){
		return service.searchByTitle(phrase);
	}
}
