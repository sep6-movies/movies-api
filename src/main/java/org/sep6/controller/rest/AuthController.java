package org.sep6.controller.rest;

import org.sep6.security.AuthResponseDto;
import org.sep6.security.AuthService;
import org.sep6.service.dto.PasswordLoginDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

	private final AuthService authService;

	@PostMapping("/login")
	public ResponseEntity<AuthResponseDto> login(@RequestBody PasswordLoginDto dto) {
		AuthResponseDto auth = authService.login(dto);

		return ResponseEntity.ok()
			.header(HttpHeaders.AUTHORIZATION, auth.token())
			.body(auth);
	}
}
