package org.sep6.controller.rest;

import java.util.List;

import org.sep6.service.FavouritesService;
import org.sep6.service.dto.AddToFavouritesDto;
import org.sep6.service.dto.MovieSummaryDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/users/{userId}/favourites")
@RequiredArgsConstructor
public class FavouritesController {

	private final FavouritesService favouritesService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void addToFavourites(
		@PathVariable("userId") long userId,
		@RequestBody AddToFavouritesDto dto,
		Authentication auth) {

		favouritesService.addToFavourites(userId, dto, auth);
	}

	@GetMapping
	public ResponseEntity<List<MovieSummaryDto>> getFavourites(
			@PathVariable("userId") long userId,
			Authentication auth) {

		return ResponseEntity.of(favouritesService.getFavourites(userId, auth));
	}

	@DeleteMapping("/{movieId}")
	public void deleteFromFavourites(
		@PathVariable("userId") long userId,
		@PathVariable("movieId") String movieId,
		Authentication auth) {

		favouritesService.removeFromFavourites(userId, movieId, auth);
	}
}
