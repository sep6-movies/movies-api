package org.sep6.controller.rest;

import org.sep6.service.RatingService;
import org.sep6.service.dto.AddRatingDto;
import org.sep6.service.dto.RatingDto;
import org.sep6.service.dto.RatingSummaryDto;
import org.sep6.service.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/movies/{movieId}/ratings")
@RequiredArgsConstructor
public class RatingController {

	private final RatingService ratingService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public RatingSummaryDto addRating(
			@PathVariable("movieId") String movieId,
			@RequestBody @Valid AddRatingDto dto,
			Authentication auth) {

		return ratingService.addRating(movieId, dto, auth)
			.orElseThrow(NotFoundException::new);
	}

	@GetMapping("/{userId}")
	public ResponseEntity<RatingDto> getRating(
			@PathVariable("movieId") String movieId,
			@PathVariable("userId") long userId) {

		return ResponseEntity.of(ratingService.getRating(movieId, userId));
	}

	@PutMapping("/{userId}")
	public ResponseEntity<RatingSummaryDto> updateRating(
			@PathVariable("movieId") String movieId,
			@PathVariable("userId") long userId,
			@RequestBody @Valid RatingDto dto,
			Authentication auth) {

		return ResponseEntity.of(ratingService.updateRating(movieId, userId, dto, auth));
	}

	@DeleteMapping("/{userId}")
	public ResponseEntity<RatingSummaryDto> deleteRating(
			@PathVariable("movieId") String movieId,
			@PathVariable("userId") long userId,
			Authentication auth) {

		return ResponseEntity.of(ratingService.deleteRating(movieId, userId, auth));
	}
}