package org.sep6.controller.rest;

import org.sep6.service.UserService;
import org.sep6.service.dto.RegisterUserDto;
import org.sep6.service.dto.UpdateUserDto;
import org.sep6.service.dto.UserDetailsDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

	private final UserService service;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public UserDetailsDto register(@RequestBody @Valid RegisterUserDto dto) {
		return service.register(dto);
	}

	@GetMapping("/{userId}")
	public ResponseEntity<UserDetailsDto> getDetails(@PathVariable("userId") long userId) {
		return ResponseEntity.of(service.getDetails(userId));
	}

	@PatchMapping("/{userId}")
	public ResponseEntity<UserDetailsDto> update(
			@PathVariable("userId") long userId,
			@RequestBody @Valid UpdateUserDto dto,
			Authentication auth) {

		return ResponseEntity.of(service.update(userId, dto, auth));
	}
}
