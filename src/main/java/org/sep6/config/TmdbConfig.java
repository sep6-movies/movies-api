package org.sep6.config;

import org.sep6.service.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import feign.RequestInterceptor;
import feign.Response;
import feign.codec.ErrorDecoder;

@Configuration
@EnableFeignClients("org.sep6.persistence.tmdb")
public class TmdbConfig {

	@Bean
	public RequestInterceptor tokenAuthInterceptor(@Value("${tmdb.apikey}") String apiKey) {
		return requestTemplate -> {
			requestTemplate.header("Authorization", "Bearer " + apiKey);
		};
	}

	@Bean
	public ErrorDecoder tmdbErrorDecoder() {
		return (String key, Response resp) ->
			switch(resp.status()) {
				case 400 -> new IllegalArgumentException();
				case 404 -> new NotFoundException();
				case 409 -> new IllegalStateException();
				default -> (resp.status() > 500)
					? new ResponseStatusException(HttpStatus.BAD_GATEWAY)
					: new RuntimeException(String.format(
								"Unexpected response has been returned from TMDB API: [%d] %s %s",
								resp.status(),
								key,
								resp.request().url()));
			};
	}
}