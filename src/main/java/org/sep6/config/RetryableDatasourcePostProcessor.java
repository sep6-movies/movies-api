package org.sep6.config;

import javax.sql.DataSource;

import org.sep6.persistence.RetryableDataSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class RetryableDatasourcePostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		if(bean instanceof DataSource)
			return new RetryableDataSource((DataSource) bean);

		return bean;
	}
}
