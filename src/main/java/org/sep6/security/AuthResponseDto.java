package org.sep6.security;

import org.sep6.service.dto.UserDetailsDto;

public record AuthResponseDto(
		UserDetailsDto details,
		String token
) {}
