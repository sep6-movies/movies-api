package org.sep6.security;

import java.time.Instant;
import java.util.Optional;

import org.sep6.domain.User;
import org.sep6.persistence.jpa.UserRepository;
import org.sep6.service.dto.PasswordLoginDto;
import org.sep6.service.exception.InvalidCredentialsException;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

	private final AuthenticationManager authManager;
	private final JwtEncoder encoder;
	private final UserRepository userRepo;

	public AuthService(
			@Lazy AuthenticationManager authManager,
			JwtEncoder encoder,
			UserRepository userRepo) {

		this.authManager = authManager;
		this.encoder = encoder;
		this.userRepo = userRepo;
	}

	public AuthResponseDto login(PasswordLoginDto dto) {
		try {
			Authentication authenticate = authManager
				.authenticate(new UsernamePasswordAuthenticationToken(
							dto.username(), dto.password()));

			var details = ((UserPrincipalDto) (authenticate.getPrincipal())).getDetails();

			var now = Instant.now();

			var claims = JwtClaimsSet.builder()
				.issuer("self")
				.issuedAt(now)
				.expiresAt(now.plusSeconds(60l * 60))
				.subject(details.username())
				.claim("details", details)
				.build();

			var token = encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
			return new AuthResponseDto(details, token);

		} catch(BadCredentialsException ex) {
			throw new InvalidCredentialsException();
		}
	}

	public Optional<User> authenticated(long userId, Authentication auth) {

		return Optional.ofNullable(auth)
			.map(a -> (Jwt) a.getPrincipal())
			.map(jwt -> (long) jwt.getClaimAsMap("details").get("id"))
			.filter(pid -> userId == pid)
			.flatMap(userRepo::findById);
	}
}