package org.sep6.security;

import java.util.List;

import org.sep6.service.dto.UserDetailsDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

@Data
public class UserPrincipalDto implements UserDetails {

	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	private List<GrantedAuthority> authorities = List.of();

	private UserDetailsDto details;
	private String password;

	public UserPrincipalDto(UserDetailsDto details, String password) {
		this.details = details;
		this.password = password;
	}

	public String getUsername() {
		return details.username();
	}
}
