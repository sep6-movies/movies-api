package org.sep6.service;

import org.junit.jupiter.api.Test;
import org.sep6.MoviesApiApplication;
import org.sep6.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = MoviesApiApplication.class)
@AutoConfigureMockMvc
class FavouritesServiceTest {

    @Value("${tmdb.imagePrefix}")
    private String imagePrefix;

    @Autowired
    private MockMvc mvc;

    private static final Movie TEST_MOVIE = new Movie(
            "2",
            "Ariel",
            "/ojDg0PGvs6R9xYFodRct2kdI6wC.jpg",
            "Taisto Kasurinen is a Finnish coal miner whose father has just committed suicide and who is framed for a crime he did not commit. In jail, he starts to dream about leaving the country and starting a new life. He escapes from prison but things don't go as planned...");

    @Test
    void testGetPopular() throws Exception {
        mvc.perform(get("/movies/popular").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(20)));
    }

    @Test
    void testFindById() throws Exception {
        mvc.perform(get("/movies/{movieId}", "2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(TEST_MOVIE.id())))
                .andExpect(jsonPath("$.title", is(TEST_MOVIE.title())))
                .andExpect(jsonPath("$.posterUrl", is(imagePrefix + TEST_MOVIE.posterPath())))
                .andExpect(jsonPath("$.description", is(TEST_MOVIE.description())));
    }
}