package org.sep6;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class MoviesApiApplicationTests {

	@Test
	void contextLoads() {}
}
